package com.example.horsejob_5_db.tables;

import android.database.sqlite.SQLiteDatabase;



public class NotesTable {
	public static final String TABLE_NAME = "notes";
	public static final String COLUMN_HORSE_ID = "_id";
	public static final String COLUMN_NOTES_DATE = "date";
	public static final String COLUMN_NOTES_TEXT = "text";
	

	public static final String CREATE_TABLE = "CREATE TABLE "
			+ TABLE_NAME 
			+ "(" 
			+ COLUMN_HORSE_ID 
			+ " INTEGER," 
			+ COLUMN_NOTES_DATE 
			+ " DATETIME," 
			+ COLUMN_NOTES_TEXT 
			+ " TEXT,"
			+ " FOREIGN KEY ("+ COLUMN_HORSE_ID + ")"
			+ " REFERENCES " + HorseTable.TABLE_NAME +"(" + HorseTable.COLUMN_ID
			+"));";
			
			

	public static void onCreate(SQLiteDatabase db){
		db.execSQL(CREATE_TABLE);
	}

	public static void onUpgrade(SQLiteDatabase db, int oldVersion, 
			int newVersion){
		db.execSQL("DROP TABLE IF EXISTS " +TABLE_NAME);
		onCreate(db);

	}


}
