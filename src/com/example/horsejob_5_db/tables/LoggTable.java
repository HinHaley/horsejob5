package com.example.horsejob_5_db.tables;

import android.database.sqlite.SQLiteDatabase;



public class LoggTable {
	public static final String TABLE_NAME = "logg";
	public static final String COLUMN_HORSE_ID = "_id";
	public static final String COLUMN_LOGG_TIMESTAMP = "timestamp";
	public static final String COLUMN_LOGG_GAIT = "gait";
	public static final String COLUMN_LOGG_LEG = "keg";
	
	public static final String CREATE_TABLE = "CREATE TABLE "
			+ TABLE_NAME 
			+ "(" 
			+ COLUMN_HORSE_ID 
			+ " INTEGER," 
			+ COLUMN_LOGG_TIMESTAMP 
			+ " DATETIME," 
			+ COLUMN_LOGG_GAIT
			+ " TINYINT,"
			+ COLUMN_LOGG_LEG
			+ " TINYINT,"
			+ " FOREIGN KEY ("+ COLUMN_HORSE_ID + ")"
			+ " REFERENCES " + HorseTable.TABLE_NAME +"(" + HorseTable.COLUMN_ID
			+"));";
	
	public static void onCreate(SQLiteDatabase db){
		db.execSQL(CREATE_TABLE);
	}
	
	public static void onUpgrade(SQLiteDatabase db, int oldVersion, 
			int newVersion){
		db.execSQL("DROP TABLE IF EXISTS " +TABLE_NAME);
		onCreate(db);
		
	}

}
