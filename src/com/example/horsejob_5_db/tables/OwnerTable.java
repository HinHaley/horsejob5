package com.example.horsejob_5_db.tables;

import android.database.sqlite.SQLiteDatabase;

public class OwnerTable {
	
	public static final String TABLE_NAME = "owners";
	public static final String COLUMN_OWNER_ID = "_id";
	public static final String COLUMN_OWNER_NAME = "ownername";
	public static final String COLUMN_OWNER_PHONE = "ownerphonenumber";
	public static final String COLUMN_OWNER_EMAIL = "owneremail";
	
	public static final String CREATE_TABLE = "CREATE TABLE "
			+ TABLE_NAME 
			+ "(" 
			+ COLUMN_OWNER_ID 
			+ " INTEGER PRIMARY KEY ," 
			+ COLUMN_OWNER_NAME 
			+ " TEXT," 
			+ COLUMN_OWNER_EMAIL
			+ " TEXT,"
			+ COLUMN_OWNER_PHONE 
			+ " TEXT);";
	
	public static void onCreate(SQLiteDatabase db){
		db.execSQL(CREATE_TABLE);
	}
	
	public static void onUpgrade(SQLiteDatabase db, int oldVersion, 
			int newVersion){
		db.execSQL("DROP TABLE IF EXISTS " +TABLE_NAME);
		onCreate(db);
		
	}

}
