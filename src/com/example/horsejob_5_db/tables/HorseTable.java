package com.example.horsejob_5_db.tables;

import android.database.sqlite.SQLiteDatabase;

public class HorseTable {
	
	public static final String TABLE_NAME = "horses";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_NAME = "horsename";
	public static final String COLUMN_OWNER_ID = "ownerid";
	public static final String COLUMN_HORSE_AGE = "age";
	public static final String COLUMN_HORSE_PIC = "picture";
	

	public static final String CREATE_TABLE = "CREATE TABLE "
			+ TABLE_NAME 
			+ "(" 
			+ COLUMN_ID 
			+ " INTEGER PRIMARY KEY," 
			+ COLUMN_NAME 
			+ " TEXT," 
			+ COLUMN_OWNER_ID 
			+ " TEXT,"
			+ COLUMN_HORSE_AGE
			+ " TINYINT,"
			+ COLUMN_HORSE_PIC
			+ " BLOB,"
			+ " FOREIGN KEY ("+ COLUMN_OWNER_ID + ")"
			+ " REFERENCES " + OwnerTable.TABLE_NAME +"(" + OwnerTable.COLUMN_OWNER_ID
			+"));";
			
			

	public static void onCreate(SQLiteDatabase db){
		db.execSQL(CREATE_TABLE);
	}

	public static void onUpgrade(SQLiteDatabase db, int oldVersion, 
			int newVersion){
		db.execSQL("DROP TABLE IF EXISTS " +TABLE_NAME);
		onCreate(db);

	}

}
