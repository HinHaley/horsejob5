package com.example.horsejob_5_db;

import com.example.horsejob_5_db.database.DbHelper;
import com.example.horsejob_5_db.tables.HorseTable;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class HorseActivity extends Activity implements OnClickListener{
	
	String horseName;
	String horseBirthYear;
	public static long horseId;
	static String[] horseInfo = new String[2];
	
	TextView tvHorseName;
	TextView tvHorseBirthYear;
	ListView listView;
	Button newLogg;
	
	DbHelper dbHelper;
	SQLiteDatabase db;
	Cursor cursor;
	SimpleCursorAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_horse);
		
		Intent i = getIntent();
		Bundle b = i.getExtras();
		if(b!=null) horseId = b.getLong("key1");
		
		listView = (ListView) findViewById(R.id.listView1);
		
		newLogg = (Button) findViewById(R.id.button_horse_activity_new_logg);
		newLogg.setOnClickListener(this);
		
		tvHorseName = (TextView) findViewById(R.id.text_horse_activity_horse_name);
		tvHorseBirthYear = (TextView) findViewById(R.id.text_horse_activity_horse_birthyear);
		
		dbHelper = new DbHelper(this);
		
		horseInfo = getHorseInfo();
		
		tvHorseName.setText(horseInfo[0]);
		tvHorseBirthYear.setText(horseInfo[1]);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.horse, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private String[] getHorseInfo(){
		String[] info = new String[2];
		
		db = dbHelper.getReadableDatabase();
		
		cursor = db.query(HorseTable.TABLE_NAME, new String[]{HorseTable.COLUMN_NAME,
				HorseTable.COLUMN_HORSE_AGE}, HorseTable.COLUMN_ID +" = " + horseId,
				null, null, null, null);
		
		cursor.moveToFirst();
    	info[0] = cursor.getString(cursor.getColumnIndex(HorseTable.COLUMN_NAME));
    	cursor.moveToFirst();
		info[1] = String.valueOf(cursor.getInt(cursor.getColumnIndex(HorseTable.COLUMN_HORSE_AGE)));
		
		cursor.close();
		
		db.close();
		
		return info;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.button_horse_activity_new_logg:
			Intent i = new Intent(this, LoggSettingsActivity.class);
			startActivity(i);
			break;
		}
		
	}
}
