package com.example.horsejob_5_db;

import com.example.horsejob_5_db.database.DbHelper;
import com.example.horsejob_5_db.tables.OwnerTable;

import android.app.Activity;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class AddOwner extends Activity implements OnClickListener{
	
	Button add;
	EditText ownerName;
	EditText ownerPhone;
	EditText ownerEmail;
	
	public String name;
	public String phonenr;
	public String email;

	DbHelper dbHelper;
	SQLiteDatabase db;
	
	Handler mHandler;
	Handler messageHandler;
	Handler.Callback mHandlerCallback;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_owner);
//		callBack();
		Log.i("DB", "Add owner started");
		final Activity act = this;
		dbHelper = new DbHelper(this);

		ownerName = (EditText) findViewById(R.id.editText_add_owner_name);
		ownerPhone = (EditText) findViewById(R.id.editText_add_owner_phnumber);
		ownerEmail = (EditText) findViewById(R.id.editText_add_owner_email);
		
		add = (Button) findViewById(R.id.button_add_owner_to_db);
		add.setOnClickListener(this);
		
		mHandler = new Handler(){

			@Override
			public void handleMessage(Message msg) {
				switch (msg.what){
				case 0:
					act.finish();
					Log.i("DB", "msg recieved");
					break;
				}
			 
			}};
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_owner, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.button_add_owner_to_db:
			name = ownerName.getText().toString();
			phonenr = ownerPhone.getText().toString();
			email = ownerEmail.getText().toString();
//		
			Log.i("DB", "name: " +name+ " phone: " +phonenr +" email: "+email);
			
			mHandler.post(new Runnable() {
				
				@Override
				public void run() {
					Log.i("DB", "Runnable started");
					db = dbHelper.getWritableDatabase();
					Log.i("DB", "DB opened");
					ContentValues values = new ContentValues();
					values.put(OwnerTable.COLUMN_OWNER_NAME, name);
					values.put(OwnerTable.COLUMN_OWNER_PHONE, phonenr);
					values.put(OwnerTable.COLUMN_OWNER_EMAIL, email);
					try{
						db.insert(OwnerTable.TABLE_NAME, null, values);
					}finally{
						Log.i("DB", "finally!");
						db.close();
						Log.i("DB", "DB closed");
//						act.finish();
						mHandler.sendEmptyMessage(0);
						Log.i("DB", "mHandler send message");
					}
				}
			});
			break;
		}
	}
//	private  mHandlerCallback = new Handler.Callback(){
//		
//	};
//	private void callBack(){
//		Log.i("DB", "callBack() called");
//		
//		Callback mHandlerCallback = new Handler.Callback() {
//			
//			@Override
//			public boolean handleMessage(Message msg) {
//				
//				}
//				return false;
//			}
//		};
//		
//	}
}
