package com.example.horsejob_5_db;

import com.example.horsejob_5_db.sensortag_impl.MainActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.Spinner;
import android.widget.Switch;

public class LoggSettingsActivity extends Activity implements OnClickListener, OnItemSelectedListener {
	
	NumberPicker picker;
	Spinner gaitSpinner;
	Spinner loggLengthSpinner;
	Button next;
	Switch startTimer;
	Switch loggTimer;
	
	public static int numberOfTags = 1;
	public static boolean startTimerOnOff = true;
	public static boolean loggTimerOnOff;
	public static int loggTimeLength;
	public static String gait;
	public static long horseId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_logg_settings);
		Log.i("DB", "LoggSettings started..");
		horseId = HorseActivity.horseId;
		
		next = (Button) findViewById(R.id.button_log_settings_next);
		next.setOnClickListener(this);
		
		gaitSpinner = (Spinner) findViewById(R.id.spinner_settings_activity_gait);
		ArrayAdapter<CharSequence> gaitAdapter = ArrayAdapter.createFromResource(this, R.array.gait_array,
				android.R.layout.simple_spinner_item);
		gaitAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		gaitSpinner.setAdapter(gaitAdapter);
		gaitSpinner.setOnItemSelectedListener(this);
		
		loggLengthSpinner = (Spinner) findViewById(R.id.spinner_settings_activity_timer_length);
		ArrayAdapter<CharSequence> lengthAdapter = ArrayAdapter.createFromResource(this, R.array.loggtime_length,
				android.R.layout.simple_spinner_item);
		lengthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		loggLengthSpinner.setAdapter(lengthAdapter);
		loggLengthSpinner.setOnItemSelectedListener(this);
		loggLengthSpinner.setEnabled(false);
		
		picker = (NumberPicker) findViewById(R.id.numberPicker_tags);
		picker.setMinValue(1);
		picker.setMaxValue(4);
		picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
		picker.setOnValueChangedListener(new OnValueChangeListener() {
			
			@Override
			public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
				numberOfTags = newVal;
				
			}
		});
		
		loggTimer = (Switch) findViewById(R.id.switch2);
		loggTimer.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				Log.i("DB", "Switch is set to: " +isChecked);
				if(isChecked){
					loggLengthSpinner.setEnabled(true);
				}else if(!isChecked){
					loggLengthSpinner.setEnabled(false);
				}
				
			}
		});
		startTimer = (Switch) findViewById(R.id.switch1);
		startTimer.setChecked(true);
		startTimer.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) startTimerOnOff = true;
				else startTimerOnOff = false;
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.logg_settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.button_log_settings_next:
			Intent i = new Intent(this, MainActivity.class);
			startActivity(i);
		}
		
		
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		switch (parent.getId()){
		case R.id.spinner_settings_activity_gait:
			Log.i("DB", "Gait clicked");
			gait = gaitSpinner.getSelectedItem().toString();
			Log.i("DB", gait);
			break;
			
		case R.id.spinner_settings_activity_timer_length:
			Log.i("DB", "Timer length clicked");
			loggTimeLength =Integer.parseInt(loggLengthSpinner.getSelectedItem().toString());
			Log.i("DB", "loggTimeLength= " +loggTimeLength);
			break;
		}
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}
}
