package com.example.horsejob_5_db;

import android.os.Handler;
import android.util.Log;

public class MyCountDownTimer {
	private long millisInFuture;
	private long savedMillisInFuture;
	private long countDownInterval;
	private boolean status;
	
	
	public interface CountDownCallback {
		
		public long onTick(long millisLeft);
		public void onFinish();
	}
	
	private CountDownCallback mCountDownCallback;
	
	//constructor
	public MyCountDownTimer(long pMillisInFuture, long pCountDownInterval, CountDownCallback countDownCallback){
		this.millisInFuture = pMillisInFuture;
		this.countDownInterval = pCountDownInterval;
		status = false;
		mCountDownCallback = countDownCallback;
		savedMillisInFuture = pMillisInFuture;
//		Initialize();
		
		
	}
	
	public void Start(){
		millisInFuture = savedMillisInFuture;
		Log.i("DB", "timer Start() called");
		status = true;
		Initialize();
//		
	}
	
	public void Stop(){
		status = false;
	}
	
	public long getCurrentTime(){
		return millisInFuture;
	}
	
	public void Initialize(){
		
		final Handler handler = new Handler();
		final Runnable counter = new Runnable(){

			@Override
			public void run() {
				long sec = millisInFuture/1000;
				if(status){
					if(millisInFuture <=0){
						Log.i("DB", "Calling onFinish()");
						mCountDownCallback.onFinish();
						status = false;
												
					}else{
						Log.i("DB", Long.toString(sec) + " seconds remain");
						millisInFuture -= countDownInterval;
						handler.postDelayed(this, countDownInterval);
						Log.i("DB", "Calling onTick");
						mCountDownCallback.onTick(millisInFuture);
					}
					
				}else{
					Log.i("DB", Long.toString(millisInFuture) + " seconds remain and timer has stopped!");
                    handler.postDelayed(this, countDownInterval);
				}
			}
			
		};
		handler.postDelayed(counter, countDownInterval);
	}
}
