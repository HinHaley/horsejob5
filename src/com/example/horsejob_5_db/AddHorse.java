package com.example.horsejob_5_db;

import com.example.horsejob_5_db.database.DbHelper;
import com.example.horsejob_5_db.tables.HorseTable;
import com.example.horsejob_5_db.tables.OwnerTable;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class AddHorse extends Activity implements OnClickListener{
	
	EditText horseName;
	EditText horseBirthYear;
	Button add;
	
	public String name;
	public int birthYear;
	static long ownerId;
	
	DbHelper dbHelper;
	SQLiteDatabase db;
	
	Handler mHandler;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_horse);
		Intent intent = getIntent();
		Bundle b = intent.getExtras();
		
		if(b!=null) ownerId = b.getLong("key");
		
		final Activity act = this;
		dbHelper = new DbHelper(this);
		
		Log.i("DB","AddHorse started");
		
		horseName = (EditText) findViewById(R.id.editText_add_horse_name);
		horseBirthYear = (EditText) findViewById(R.id.editText_add_horse_birthyear);
		add = (Button) findViewById(R.id.button_add_horse_to_db);
		add.setOnClickListener(this);
		
		mHandler = new Handler(){

			@Override
			public void handleMessage(Message msg) {
				switch (msg.what){
				case 0:
					Log.i("DB", "Msg Recieved");
					act.finish();
				}
			}
			
		};
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_horse, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.button_add_horse_to_db:
			Log.i("DB","Add horse pressed..");
			name = horseName.getText().toString();
			birthYear = Integer.parseInt(horseBirthYear.getText().toString());
			
			mHandler.post(new Runnable() {
				
				@Override
				public void run() {
					Log.i("DB", "Runnable started");
					try{
						db = dbHelper.getWritableDatabase();
					}catch (SQLException e){
						Log.i("DB", e.toString());
					}
					Log.i("DB", "DB opened");
					ContentValues values = new ContentValues();
					values.put(HorseTable.COLUMN_NAME, name);
					values.put(HorseTable.COLUMN_HORSE_AGE, birthYear);
					values.put(HorseTable.COLUMN_OWNER_ID, ownerId );
					try{
						if(db.insert(HorseTable.TABLE_NAME, null, values)==-1)
							Log.i("DB", "Error inserting");
					}finally{
						Log.i("DB", "finally!");
						db.close();
						Log.i("DB", "DB closed");
//						act.finish();
						mHandler.sendEmptyMessage(0);
						Log.i("DB", "mHandler send message");
					}
				}
			});
			
			break;
		}
		
	}
}
