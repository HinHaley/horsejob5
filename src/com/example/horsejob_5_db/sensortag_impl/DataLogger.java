package com.example.horsejob_5_db.sensortag_impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;








import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

public class DataLogger {

	
	//Context
	Context mContext;
	
	//Message string
	static String message = new String();
	
	//Arraylists for storing values from sensors
	static ArrayList<String> SensorTagVF = new ArrayList<String>();
	static ArrayList<String> SensorTagHF = new ArrayList<String>();
	static ArrayList<String> SensorTagVB = new ArrayList<String>();
	static ArrayList<String> SensorTagHB = new ArrayList<String>();
	
	//Runnable
	private WriteRunnable mWriteRunnable;
	
	//Flags
	public static boolean loggOn;
	
	//Constructor
	public DataLogger(Context context){
		mContext = context;
	}
	
	//Runnable
	private class WriteRunnable implements Runnable{
		private ArrayList<String> sensorTag;
		private String tagName;
		private String dataTag;
		private String time;
			
			
			public WriteRunnable(ArrayList<String> sensorTag, String tagName, String dataTag, String time){
				Log.i("HJ", "WriteRunnable called.");
				this.sensorTag = sensorTag;
				this.tagName = tagName;
				this.dataTag = dataTag;
				this.time = time;
			}

			@Override
			public void run() {
				Log.i("HJ", "Calling saveToFile..");
				saveToFile(sensorTag, tagName, dataTag , time);
			}
			
		}
	public static class LogThread extends Thread {
			
		public static Handler mHandlerDataLog;
		private boolean loggOn;
			
		public void setLoggingOn(boolean loggOn){
			Log.i("HJ", "setLoggingon called...");
			clearLists();
			this.loggOn = loggOn;
		}
		
		@SuppressLint("HandlerLeak")
		@Override
		public void run(){
			Looper.prepare();
			mHandlerDataLog = new Handler() {

				public void handleMessage(Message msg) {

					if(loggOn){

						switch (msg.what){

						case MainActivity.VF:
							message = (String) msg.obj; 
							SensorTagVF.add(message);
							Log.i("HJ","Adding post to VF list");
							break;

						case MainActivity.HF:
							message = (String) msg.obj; 
							SensorTagHF.add(message);
							Log.i("HJ","Adding post to HF list");
							break;

						case MainActivity.VB:
							message = (String) msg.obj; 
							SensorTagVB.add(message);
							Log.i("HJ","Adding post to VB list");
							break;

						case MainActivity.HB:
							message = (String) msg.obj; 
							SensorTagHB.add(message);
							Log.i("HJ","Adding post to HB list");
							break;
						}
					}}};
					
					Looper.loop();	

		}
	}	

		public static void clearLists(){
			SensorTagVF.clear();
			SensorTagHF.clear();
			SensorTagVB.clear();
			SensorTagHB.clear();
		}
		
		public void setLoggingOff(int count, String tag){
			
			Log.i("HJ", "setLoggingOff called..");	
			//Turning off reading of messages
			
			loggOn = false;
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
			Date d = new Date();
			String time = sdf.format(d);
			
			if(!SensorTagVF.isEmpty()){
				mWriteRunnable = new WriteRunnable(SensorTagVF, "VF", tag, time);
				LogThread.mHandlerDataLog.post(mWriteRunnable);
			}
			
			if(!SensorTagHF.isEmpty()){
				mWriteRunnable = new WriteRunnable(SensorTagHF, "HF", tag, time);
				LogThread.mHandlerDataLog.post(mWriteRunnable);
			}
			
			if(!SensorTagVB.isEmpty()){
				mWriteRunnable = new WriteRunnable(SensorTagVB, "VB", tag, time);
				LogThread.mHandlerDataLog.post(mWriteRunnable);
			}
			
			if(!SensorTagHB.isEmpty()){
				mWriteRunnable = new WriteRunnable(SensorTagHB, "HB", tag, time);
				LogThread.mHandlerDataLog.post(mWriteRunnable);
			}
			
//			switch (count){
//			
//			case 4:
//				mWriteRunnable = new WriteRunnable(SensorTagVF, "tagOne", tag, time);
//				LogThread.mHandlerDataLog.post(mWriteRunnable);
//				mWriteRunnable = new WriteRunnable(SensorTagHF, "tagTwo", tag, time);
//				LogThread.mHandlerDataLog.post(mWriteRunnable);
//				mWriteRunnable = new WriteRunnable(SensorTagVB, "tagThree",tag, time);
//				LogThread.mHandlerDataLog.post(mWriteRunnable);
//				mWriteRunnable = new WriteRunnable(SensorTagHB, "tagFour",tag, time);
//				LogThread.mHandlerDataLog.post(mWriteRunnable);
//				break;
//				
//			case 3:
//				mWriteRunnable = new WriteRunnable(SensorTagVF,"tagOne", tag, time);
//				LogThread.mHandlerDataLog.post(mWriteRunnable);
//				mWriteRunnable = new WriteRunnable(SensorTagHF,"tagTwo",tag, time);
//				LogThread.mHandlerDataLog.post(mWriteRunnable);
//				mWriteRunnable = new WriteRunnable(SensorTagVB,"tagThree",tag, time);
//				LogThread.mHandlerDataLog.post(mWriteRunnable);
//				break;
//				
//			case 2:
//				mWriteRunnable = new WriteRunnable(SensorTagVF, "tagOne",tag, time);
//				LogThread.mHandlerDataLog.post(mWriteRunnable);
//				mWriteRunnable = new WriteRunnable(SensorTagHF, "tagTwo",tag, time);
//				LogThread.mHandlerDataLog.post(mWriteRunnable);
//				break;
//				
//			case 1: 
//				mWriteRunnable = new WriteRunnable(SensorTagVF, "tagOne",tag, time);
//				LogThread.mHandlerDataLog.post(mWriteRunnable);
//				break;
//
//			}
			
		
		}
		public void saveToFile(ArrayList<String> sensorTag,  String tagName, String fileTag, String time){
		
			Log.i("LIST", "saveToFile called");
			
			String mPath = new String();
			String fileName = new String();
			String fileHeader = new String();
			String dataToString = new String();

			
			if(fileTag != null) mPath = time+fileTag+"-"+"/";
			else mPath = time+"/";

			if(tagName != null) fileName = tagName+ " "+ time+".csv";
			else fileName = time+".csv";

			fileHeader = ",,ACCEL,,,GYRO \n"+"TIME,READING NO.,X,Y,Z,X,Y,Z\n";

			try{

				File mPathFile = new File (Environment.getExternalStorageDirectory().toString()
						+"/Acceltag/logs/"+mPath);
				if(!mPathFile.isDirectory()){
					mPathFile.mkdirs();
					Log.i("LIST", mPathFile.toString()+" is not a directory");
				}

				File mFile = new File(mPathFile,fileName);

				mFile.createNewFile();

				mFile.getParentFile().mkdirs();

				BufferedOutputStream fOut = new BufferedOutputStream(new FileOutputStream(mFile, true));
				OutputStreamWriter mOutWriter = new OutputStreamWriter(fOut);
				
				mOutWriter.append(fileHeader);
				
				for(int i = 0; i < sensorTag.size(); i++){
					dataToString = sensorTag.get(i);
					mOutWriter.append(dataToString);
				}
				
				
				
				mOutWriter.flush();
				mOutWriter.close();
				fOut.close();
				
				MediaScannerConnection.scanFile(mContext, new String[] { mFile.getAbsolutePath() }, null, null);
				MainActivity.mHandler.sendEmptyMessage(5);
				
				Log.i("HJ", "File Closed");
			
			}catch (Exception e){
				
				e.printStackTrace();
				Log.i("HJ",e.toString());
			}
		}
}

