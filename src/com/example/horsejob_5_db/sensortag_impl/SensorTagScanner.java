package com.example.horsejob_5_db.sensortag_impl;

import java.util.ArrayList;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

public class SensorTagScanner {
	
	// Private Variables
	private final String TAG = "SensorTagScanner";
	private final BluetoothManager mBluetoothManager;
	private final BluetoothAdapter mBluetoothAdapter;
	private int mScanTime = 5000;						// Default scan time = 5 seconds
	
	// Private Objects
	private ArrayList<BluetoothDevice> mDeviceList;
	private Handler mHandler;
	private ScanCallback mScanCallback;
	
	// Public Callback
	public interface ScanCallback {
		public void onScanComplete(ArrayList<BluetoothDevice> deviceList);
	}
	
	// Constructor
	public SensorTagScanner(Context context) {
		mDeviceList = new ArrayList<BluetoothDevice>();
		mBluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);		
		mBluetoothAdapter = mBluetoothManager.getAdapter();
	}
	
	public void scan(int milliseconds, ScanCallback sc) {
		mScanTime = milliseconds;
		mScanCallback = sc;
		
		
		// Return error if Bluetooth Adapter not enabled
		if (!mBluetoothAdapter.isEnabled()) {
			throw new Error("Bluetooth not enabled");
		}
				
		// Scan callback
		final LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
    		
    		@Override
    		public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
    			
    			// Populate device list (if device name equals "SensorTag")
    			if (!mDeviceList.contains(device) && device.getName() != null) {	
    				mDeviceList.add(device);
    				Log.d(TAG, "Device found: " + device.getName() + " " + device.getAddress());
    			}
    		}};
    		
    		// Runnable to start scan
    		
    		Runnable start = new Runnable() {

    			@Override
    			public void run() {
    				Log.d(TAG, "Scan started (" + mScanTime + " ms)");
    				mBluetoothAdapter.startLeScan(mLeScanCallback);
    			}};

    		// Runable to stop scan
    		Runnable stop = new Runnable() {

				@Override
				public void run() {
					Log.d(TAG, "Scan complete");
					mBluetoothAdapter.stopLeScan(mLeScanCallback);
					mScanCallback.onScanComplete(mDeviceList);
				}};
    		
		mHandler = new Handler();
		
		// Clear device list
		mDeviceList.clear();

		// Start scan in new thread
		mHandler.post(start);
		
		// Stop scan
    	mHandler.postDelayed(stop, mScanTime);
	}
}
	