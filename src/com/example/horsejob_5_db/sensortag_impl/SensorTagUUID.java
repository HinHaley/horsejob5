package com.example.horsejob_5_db.sensortag_impl;

import java.util.UUID;

public class SensorTagUUID {
	
	// General
	public static final UUID CONFIG_DESCRIPTOR = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
	
    // Keys
    public static final UUID KEY_SERVICE = UUID.nameUUIDFromBytes(new byte[] {(byte)0xFFE0});
    public static final UUID KEY_DATA = UUID.nameUUIDFromBytes(new byte[] {(byte)0xFFE1});
	
	// Accelerometer
	public static final UUID ACC_SERVICE = UUID.fromString("f000aa10-0451-4000-b000-000000000000");
    public static final UUID ACC_DATA = UUID.fromString("f000aa11-0451-4000-b000-000000000000");
    public static final UUID ACC_CONFIG = UUID.fromString("f000aa12-0451-4000-b000-000000000000");
    public static final UUID ACC_PERIOD = UUID.fromString("f000aa13-0451-4000-b000-000000000000");
    
    // Gyroscope
	public static final UUID GYR_SERVICE = UUID.fromString("f000aa50-0451-4000-b000-000000000000");
    public static final UUID GYR_DATA = UUID.fromString("f000aa51-0451-4000-b000-000000000000");
    public static final UUID GYR_CONFIG = UUID.fromString("f000aa52-0451-4000-b000-000000000000");
    public static final UUID GYR_PERIOD = UUID.fromString("f000aa53-0451-4000-b000-000000000000");
    
}
