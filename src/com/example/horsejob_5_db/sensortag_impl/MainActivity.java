package com.example.horsejob_5_db.sensortag_impl;

import com.example.horsejob_5_db.LoggSettingsActivity;
import com.example.horsejob_5_db.MyCountDownTimer;
import com.example.horsejob_5_db.MyCountDownTimer.CountDownCallback;
import com.example.horsejob_5_db.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener{
	
	/*Finals*/
	private static final int REQUEST_ENABLE_BT = 1;
	private static final int START_TIMER = 6000;
	private static final int PERIOD_TIME = 5000;
	
	private static final int B_STATE_ENABLED = 1;
	private static final int B_STATE_CONNECTED = 2;
	private static final int B_STATE_DISABLED = 3;
	
	public static final int VF = 0;
	public static final int HF = 1;
	public static final int VB = 2;
	public static final int HB = 3;
	
	private static final String DEVICE_NAME = "SensorTag";
	public static final String[] HOOVES = new String[] {"VF","HF","VB","HB"};
	
	/*Variables to store info from settings activity*/
	public static String gait;
	public static long horseId;
	public static int loggTimeLength;
	public static boolean startTimerOnOff;
	public static boolean loggTimerOnOff;
	public static int numberOfTags;
	
	/*Count down timer*/
	private MyCountDownTimer mCountDownTimer;
	private CountDownCallback mCounterCallback;
	
	/*Variables for SensorTag handling*/
	public int mID;
	public int mHoof;
	private static int tagId = 0;
	
	/*Flags and state holders*/
	private static boolean[] isButtonConnected = {false,false,false,false};
	private static int[] b_state = new int[4];
	
	/*SensorTag wrapper initializers*/
	private SensorTag.SensorCallback mSensorCallback;
	public SensorTagScanner mSensorTagScanner;
	public SensorTag[] mSensorTag;
	
	/*BLE initializers*/
	public BluetoothDevice mDevice;
	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothManager manager;
	private BluetoothAdapter.LeScanCallback mLeScanCallback;
	
	/*Handler*/
	static Handler mHandler;
	private Handler.Callback mHandlerCallback;
	
	/*Variables*/
	public static int mDeviceCount; 
	
	/*Views*/
	public Button[] b = new Button[4];
	public TextView tv1;
	public Button startLog;
	
	/*Progress dialog*/
	public ProgressDialog mProgressDialog;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main2);
		
		/*Create call backs */
		callbacks();
		
		/*Get info from settings activity*/
		setHorseInfo();
		
		/*Initiate count down timer for start timer*/
		mCountDownTimer = new MyCountDownTimer(START_TIMER, 1000, mCounterCallback);
		
		// Progress bar
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
    	mProgressDialog.setCancelable(true);
    	mProgressDialog.setMessage("Scanning");
    	
    	// Initiating SensorTag array
		mSensorTag = new SensorTag[numberOfTags];
		
		/*Initiate BlueTooth manager and adapter for scanning*/
		manager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
		mBluetoothAdapter = manager.getAdapter();
		
		/*Initiate handler for messages*/
		mHandler = new Handler(mHandlerCallback);
		
		tagId = 0;
		
		Log.i("DB", "Number of tags: "+numberOfTags +" Size of array: " +mSensorTag.length + " tagId: " +tagId);
		
		if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		}
		

		tv1 = (TextView) findViewById(R.id.tv1);
		tv1.setText(gait +" , " +horseId +" , " +loggTimeLength);
		
		 //Setting up connect buttons
        b[0] = (Button) findViewById(R.id.connectButtonVF);
        b[1] = (Button) findViewById(R.id.connectButtonHF);
        b[2] = (Button) findViewById(R.id.connectButtonVB);
        b[3] = (Button) findViewById(R.id.connectButtonHB);

        //Set onClickListeners
        for(int i = 0; i<4; i++){
        	b[i].setOnClickListener(this);
        	b[i].setEnabled(true);
        	b_state[i] = B_STATE_ENABLED;
        }
        
        //Setting up startLog-button
		startLog = (Button) findViewById(R.id.buttonStartLog);
		startLog.setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		Log.i("DB","MainActivity onResume()..");
		tagId = 0;
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	
	/*onClick listener for buttons*/
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.connectButtonVF:
			switch (b_state[0]){
			case B_STATE_ENABLED:
				Log.i("DB", "tagId on buttonClick: " +tagId);
				mHoof = VF;
				scanForDevices();
				break;
			case B_STATE_CONNECTED:
				break;
			case B_STATE_DISABLED:
				break;
			}
			break;
			
		case R.id.connectButtonHF:
			switch (b_state[1]){
			case B_STATE_ENABLED:
				Log.i("DB", "tagId on buttonClick: " +tagId);
				mHoof = HF;
				scanForDevices();
				break;
			case B_STATE_CONNECTED:
				break;
			case B_STATE_DISABLED:
				break;
			}
			break;
			
		case R.id.connectButtonVB:
			switch (b_state[2]){
			case B_STATE_ENABLED:
				Log.i("DB", "tagId on buttonClick: " +tagId);
				mHoof = VB;
				scanForDevices();
				break;
			case B_STATE_CONNECTED:
				break;
			case B_STATE_DISABLED:
				break;
				
			}
			break;
			
		case R.id.connectButtonHB:
			switch (b_state[3]){
			case B_STATE_ENABLED:
				Log.i("DB", "tagId on buttonClick: " +tagId);
				mHoof = HB;
				scanForDevices();
				break;
			case B_STATE_CONNECTED:
				break;
			case B_STATE_DISABLED:
				break;
				
			}
			break;
			
		case R.id.buttonStartLog:
//			if(startTimerOnOff){
//				mHandler.post(new Runnable(){
//
//					@Override
//					public void run() {
//						mCountDownTimer.Start();
//					}
//
//				});
//			}
			for(int i = 0; i < mSensorTag.length; i++){
				if(mSensorTag[i] != null){
				Log.i("DB", "\n\n****\n\n" +
						"mSensorTag address: "+mSensorTag[i].getAddress()
						+ "\nHoof: "+HOOVES[mSensorTag[i].getHoof()] + 
						"\nID: " + mSensorTag[i].getID()+"\n****");
				}
				
			}
			break;
		
		}
		
	}
	
	private void scanForDevices(){
		
		Log.i("DB", "Calling startLeScan..");
		mProgressDialog.setMessage("Scanning...");
		mProgressDialog.show();
		mBluetoothAdapter.startLeScan(mLeScanCallback);
		
	}
	
	private void connectToDevice(int ID, int hoof){
		
		mID = ID;
		mHoof = hoof;
		
		mSensorTag[mID] = new SensorTag(getApplicationContext(), mDevice, mID, mHoof,manager);
		
		mHandler.post(new Runnable() {
    		
			@Override
			public void run() {
//				Log.i("DB", "calling connect with mID: " +mID);
				mSensorTag[mID].connect(new SensorTag.ConnectionCallback() {
					
					@Override
					public void onConnectionSuccess(int ID, int Hoof) {
						
//						Log.i("DB", "Calling init with mID: " + ID);
						init(ID);
						
					}
					
					@Override
					public void onConnectionFailure(int ID, int Hoof) {
						int msg = Hoof;
						
						if(!mSensorTag[ID].isGattNull()){
							Log.i("DB", "gatt is not null, disconnecting mSensorTag["+ID+"]...");
							mSensorTag[ID].disconnect();
							mSensorTag[ID] = null;
						}
						mHandler.sendMessage(Message.obtain(null, 4, msg));
						
					}
				});
			}});
		
	}
	
	private void init(int ID){
		mSensorTag[ID].initSensor(SensorTag.ACCELEROMETER, SensorTag.ACC_ENABLE_8G, 
				PERIOD_TIME, mSensorCallback);
	}
	
	private void callbacks() {
    	
		mHandlerCallback = new Handler.Callback() {
			
			@Override
			public boolean handleMessage(Message msg) {
				
				for(int i = 0; i < 4; i++) {
					if(msg.what == i) {
						
						//Handling messages for when a tag gets connected.
						switch (i){
						case 0:
							Log.i("DB", "Case 0..");
							
							mProgressDialog.setMessage("Device connected..");
							tagId++;
							
							Log.i("DB", "tagID set to: " +tagId);
							
							setDeviceConnected(i);
							dismissProgressDialog();
							break;
						case 1:
							Log.i("DB", "Case 1..");
							
							mProgressDialog.setMessage("Device connected..");
							tagId++;
							
							Log.i("DB", "tagID set to: " +tagId );
							
							setDeviceConnected(i);
							dismissProgressDialog();
							break;
						case 2:
							Log.i("DB", "Case 2..");
							mProgressDialog.setMessage("Device connected..");
							tagId++;
							
							Log.i("DB", "tagID set to: " +tagId);
							
							setDeviceConnected(i);
							dismissProgressDialog();
							break;
						case 3:
							Log.i("DB", "Case 3..");
							mProgressDialog.setMessage("Device connected..");
							tagId++;
							Log.i("DB", "tagID set to: " +tagId);
							
							setDeviceConnected(i);
							dismissProgressDialog();
						}
						
					//Handling messages for when a tag gets disconnected.
					}else if(msg.what == 4){
						int tag = (Integer) msg.obj;
						
						if(mProgressDialog.isShowing()){
							mProgressDialog.setMessage("Tag on hoof: " +HOOVES[tag] +" disconnected");
							dismissProgressDialog();
						}else{
							mProgressDialog.show();
							mProgressDialog.setMessage("Tag on hoof: " +HOOVES[tag] +" disconnected");
							dismissProgressDialog();
						}
						
						switch(tag){
						case 0:
							Log.i("DB","*Calling setDeviceDisconnected with tag: " +tag);
							setDeviceDisconnected(tag);
							break;
						case 1:
							Log.i("DB","**Calling setDeviceDisconnected with tag: " +tag);
							setDeviceDisconnected(tag);
							break;
						case 2:
							Log.i("DB","***Calling setDeviceDisconnected with tag: " +tag);
							setDeviceDisconnected(tag);
							break;
						case 3:
							Log.i("DB","****Calling setDeviceDisconnected with tag: " +tag);
							setDeviceDisconnected(tag);
							break;
						}
						Log.i("DB", "Tag with hoof: " +tag +" disconnected");
						break;
					}
				}
				return false;
			}
		};
    	
		mSensorCallback = new SensorTag.SensorCallback() {
			
			@Override
			public void onValueReceived(int ID, int hoof, int sensor, byte[] values) {
//				Log.i("DB", "ID: "+ ID +" Hoof: " +hoof  + " Address " + mSensorTag[ID].getAddress() + " \n\n");
				if(sensor == SensorTag.ACCELEROMETER) {
					int[] temp = new int[4];
					
					for (int i = 0; i < 4; i++) {
						temp[i] = values[15+i] & 0xFF;
					}
					long timestamp = (temp[0] << 24 | temp[1] << 16 | temp[2] << 8 | temp[3]);
					Log.i("DB", "Hoof: \n" +HOOVES[hoof]+ "\nAddress: " +mSensorTag[ID].getAddress());
					Log.i("DB", "New reading (array length: " + values.length + ")");
					for (int i = 0; i <= 14; i += 3 ) {
						Log.i("DB", "X: " + values[i+0] + " Y: " + values[i+1] + " Z: " + values[i+2]);
					}
					Log.i("DB", "Timestamp: " + timestamp);
					Log.i("DB", "\n\n");
				}
				
				
				
				
//				Log.i("DB", "Value recieved, ID: " + ID +" hoof: "+ hoof  +" address "+ mSensorTag[ID].getAddress());
			}
			
			@Override
			public void onSetupComplete(int ID, int Hoof, int sensor) {
				Log.d("DB", "Setup complete"+ " " + ID +" "+ Hoof+"\n4. *** mSensorTag = " +mSensorTag[ID].getAddress());
				
				isButtonConnected[Hoof] = true;
				mHandler.sendEmptyMessage(Hoof);
			}
		};
		
		mLeScanCallback = new LeScanCallback() {

			@Override
			public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
				if(DEVICE_NAME.equals(device.getName())){
					for(int i = 0; i < mSensorTag.length; i++)
						if(mSensorTag[i] != null){
							Log.i("DB","1. *** device = " +device.getAddress());
							Log.i("DB","2. *** sensortag = " +mSensorTag[i].getAddress());

							if(mSensorTag[i].getAddress() == device.getAddress()){
								mSensorTag[i].disconnect();
								Log.i("DB", ">>> Disconnecting mSensorTag["+i+"] with address: " + mSensorTag[i].getAddress()+" <<<");
								tagId = getLastId();
								break;
							}
						}else{
							tagId = getLastId();
						}
					
					mDevice = device;

					Log.i("DB","3. *** mDevice = "+ mDevice.getAddress());

					mBluetoothAdapter.stopLeScan(mLeScanCallback);
					mProgressDialog.setMessage("Device found, connecting...");


					connectToDevice(tagId, mHoof);
				}

			}
		};
			
		mCounterCallback = new CountDownCallback() {

			@Override
			public long onTick(long millisLeft) {
				Log.i("DB", "Time remaining: " +millisLeft);
				
				startLog.setText(Long.toString(millisLeft/1000));
				return 0;
			}

			@Override
			public void onFinish() {
				Log.i("DB", "Timer DONE");
				startLog.setText("Timer stopped");
			}
		};	
	}
	
	private void dismissProgressDialog(){
		mHandler.postDelayed(new Runnable(){

			@Override
			public void run() {
				mProgressDialog.dismiss();
			}
			
		}, 1000);
		
	}

	private boolean checkIfTagsConnected(){
				
		boolean aTagIsConnected = false;
		
		for(int i = 0; i < mSensorTag.length; i++){
			if(mSensorTag[i] == null){
				Log.i("Db","Error mSensorTag "+i+" is null");
			}else if(mSensorTag[i] != null && mSensorTag[i].isConnected){
				aTagIsConnected = true;
				break;
			}
			else if(mSensorTag[i] != null && !mSensorTag[i].isConnected){
				aTagIsConnected = false;
			}
		}
		Log.i("DB", "Returns: " +aTagIsConnected);
		return aTagIsConnected;
	}
	
	private void setButtonEnabled(int which){
		Log.i("DB", "setButtonEnabled called for id = "  +which);
		b[which].setBackgroundResource(R.drawable.button_unpressed);
		b[which].setEnabled(true);
		isButtonConnected[which] = false;
		b_state[which] = B_STATE_ENABLED;
		
	}
	
	private void disconnectGatts(){
		for(int i = 0; i < mSensorTag.length; i++){
			if(mSensorTag[i] == null){
				Log.i("DB", "SensorTag "+i+ " = null");
			}
			else if(mSensorTag[i].isConnected && mSensorTag[i] != null){
			mSensorTag[i].disconnect();
			
			}
		}
	}
	
	private int getLastId(){
		int id = 0;
		for(int i = 0; i < mSensorTag.length; i++){
			if(mSensorTag[i] != null){
				if(!mSensorTag[i].isConnected){
					id = mSensorTag[i].getID();
					break;
				}	
			}else{
				id = i;
				break;
			}
		}
		Log.i("DB", "last available id: "+ id);
		return id;
	}

	private void setDeviceConnected(int which){
		
		b[which].setBackgroundResource(R.drawable.button_busy);
		b[which].setText(HOOVES[which] +" Connected.");
		b_state[which] = B_STATE_CONNECTED;
		
		if(tagId >= numberOfTags){
			Log.i("DB", "tagID >= numberOfTags");
			for(int r = 0; r < 4; r++){
				if(!isButtonConnected[r]){
					Log.i("DB", "Setting b["+r+"] to disabled");
					b[r].setBackgroundResource(R.drawable.button_disabled);
					b_state[r] = B_STATE_DISABLED;
				}
			}
		}
	}
	
	private void setDeviceDisconnected(int which){
		
		b[which].setBackgroundResource(R.drawable.button_unpressed);
		b[which].setText(HOOVES[which]);
		b_state[which] = B_STATE_ENABLED;
		
		/*If no tags are connected, disconnect all from  *
		 * gatt, reset tagId and set all buttons enabled */
		
		if(!checkIfTagsConnected()){
			disconnectGatts();
			tagId = 0;
			for(int c = 0; c < 4; c++){
				Log.i("DB", "calling setBUttonEnabled with: " +c);
				setButtonEnabled(c);
			}
			
		/*Else get the first available Id.*/	
		
		}else{
			tagId = getLastId();
			for(int i = 0; i < 4; i++){
				if(b_state[i] != B_STATE_CONNECTED){
					setButtonEnabled(i);
				}
			}
		}
	}

	private void setHorseInfo(){
		
		horseId = LoggSettingsActivity.horseId;
		gait = LoggSettingsActivity.gait;
		loggTimeLength = LoggSettingsActivity.loggTimeLength;
		loggTimerOnOff = LoggSettingsActivity.loggTimerOnOff;
		startTimerOnOff = LoggSettingsActivity.startTimerOnOff;
		numberOfTags = LoggSettingsActivity.numberOfTags;
		
	}
}