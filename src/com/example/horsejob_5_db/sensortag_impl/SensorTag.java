package com.example.horsejob_5_db.sensortag_impl;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.util.Log;

public class SensorTag {

	// Public Constants - Services
	public static final int KEYS = 0;
	public static final int ACCELEROMETER = 1;
	public static final int GYROSCOPE = 5;
	
	// Public Constants - Config
	public static int DISABLE = 0;
	public static int ENABLE = 1;
	
	public static int ACC_ENABLE_2G = 1;
	public static int ACC_ENABLE_4G = 2;
	public static int ACC_ENABLE_8G = 3;
	
	public static int GYR_ENABLE_X = 1;
	public static int GYR_ENABLE_Y = 2;
	public static int GYR_ENABLE_XY = 3;
	public static int GYR_ENABLE_Z = 4;
	public static int GYR_ENABLE_XZ = 5;
	public static int GYR_ENABLE_YZ = 6;
	public static int GYR_ENABLE_XYZ = 7;
	
	// Public Variables
	public boolean isConnected = false;
	
	// Public callbacks
	public interface ConnectionCallback {
		public void onConnectionSuccess(int ID, int Hoof);
		public void onConnectionFailure(int ID, int Hoof);
	}

	public interface SensorCallback {
		public void onValueReceived(int ID, int Hoof, int sensor, byte[] values);
		public void onSetupComplete(int ID, int Hoof, int sensor);
	}
	
	// Private Constants
	private final String TAG = "SensorTag";
	
	// Private Variables
	
	private int mSensor;
	private int mConfig;
	private int mPeriod;
	private int mID;
	private int mState = 0;
	private int mHoof;
	private static final int RESOLUTION = 10;
	
	// Private Objects
	private final Context mContext;
	private BluetoothDevice mDevice;
	private BluetoothGatt mConnectedGatt;
	private BluetoothManager mManager;
	
	// Private Callbacks
	private ConnectionCallback mConnectionCallback;
	private SensorCallback mSensorCallback;
	
	// Constructors
	public SensorTag(Context context, BluetoothDevice device, int ID, int hoof, BluetoothManager bluetoothManager) {
		mContext = context;
		mDevice = device;
		mID = ID;
		mHoof = hoof;
		mManager = bluetoothManager;
		
	}
	
	// Public methods - heavy stuff
	public void connect(ConnectionCallback connectionCallback) {
		mConnectionCallback = connectionCallback;
		Log.d(TAG, "Connecting device " + mID + "...");
		mDevice.connectGatt(mContext, true, mGattCallback);
	}
	
	public void disconnect() {
		mConnectedGatt.close();
		isConnected = false;
		Log.d(TAG, "Device " + mID + " disconnected");
	}
	
	public void initSensor(int sensor, int config, int period, SensorCallback sensorCallback) {
		mSensor = sensor;
		mConfig = config;
		mPeriod = period/RESOLUTION;
		mSensorCallback = sensorCallback;
		config();
	}
	
	public void initSensor(int sensor, SensorCallback sensorCallback) {
		mSensor = sensor;
		mSensorCallback = sensorCallback;
	}
	
	// Public methods - simple stuff
	public String getAddress() {
		return mDevice.getAddress();
	}
	
	public void setID(int ID) {
		mID = ID;
	}
	
	public int getID() {
		return mID;
	}
	
	public int getHoof(){
		return mHoof;
	}
	
	public boolean isTagConnected(){
		boolean con = true;
		Log.i("DB", "isTagConnected called..");
		
		try{
			switch (mManager.getConnectionState(mDevice, BluetoothProfile.GATT)){
			
			case BluetoothProfile.STATE_CONNECTED:
				Log.i("DB", "1");
				con = true;
				break;
			case BluetoothProfile.STATE_DISCONNECTED:
				Log.i("DB", "1");
				con = false;
				break;
			default:
				con = false;
			}
		}catch (Exception e){
			Log.i("DB", e.toString());
		}
		Log.i("DB", "2");
		return con;
	}
	
	public boolean isGattNull(){
		boolean isNull = false;
		if(mConnectedGatt == null)
			isNull = true;
		else if(mConnectedGatt != null)
			isNull = false;
		
		return isNull;
	}
	// Private sensor methods
	private void config() {
		
		BluetoothGattCharacteristic c = null;
				
		// Get characteristic
		switch (mSensor) {
		
		case ACCELEROMETER:
			c = mConnectedGatt.getService(SensorTagUUID.ACC_SERVICE).getCharacteristic(SensorTagUUID.ACC_CONFIG);
			break;
		
		case GYROSCOPE:
			c = mConnectedGatt.getService(SensorTagUUID.GYR_SERVICE).getCharacteristic(SensorTagUUID.GYR_CONFIG);
			break;
		}
		
		// Set value
		c.setValue(new byte[] {(byte) mConfig} );
		
		// Write characteristic
		mConnectedGatt.writeCharacteristic(c);
	}
	
	private void setPeriod() {
		
		if(mPeriod == 0) {
			mState++;
			return;
		}
		BluetoothGattCharacteristic c = null;

		// Get characteristic
		switch (mSensor) {
		
		case ACCELEROMETER:
			c = mConnectedGatt.getService(SensorTagUUID.ACC_SERVICE).getCharacteristic(SensorTagUUID.ACC_PERIOD);
			break;
			
		case GYROSCOPE:
			c = mConnectedGatt.getService(SensorTagUUID.GYR_SERVICE).getCharacteristic(SensorTagUUID.GYR_PERIOD);
			break;
		}

		// Set value
		c.setValue(new byte[] {(byte) mPeriod});
		
		// Write characteristic 
		mConnectedGatt.writeCharacteristic(c);
		
		// Advance state machine
		mState++;
	}
	
	private void enableNotifications() {
		
		BluetoothGattCharacteristic c = null;
		
		// Get characteristic
		switch (mSensor) {
			
			case ACCELEROMETER:
				c = mConnectedGatt.getService(SensorTagUUID.ACC_SERVICE).getCharacteristic(SensorTagUUID.ACC_DATA);
		    	break;
			
			case GYROSCOPE:
				c = mConnectedGatt.getService(SensorTagUUID.GYR_SERVICE).getCharacteristic(SensorTagUUID.GYR_DATA);
		    	break;
		
		}

		// Get descriptor
		BluetoothGattDescriptor d = c.getDescriptor(SensorTagUUID.CONFIG_DESCRIPTOR);
		
		// Enable local notifications
		mConnectedGatt.setCharacteristicNotification(c, true);
		
		// Enable remote notifications
    	d.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
		mConnectedGatt.writeDescriptor(d);

		// Advance state machine
    	mState = 0;
	}
	
	// Private callbacks
	private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
		
			if (status == BluetoothGatt.GATT_SUCCESS && newState == BluetoothGatt.STATE_CONNECTED) {
				gatt.discoverServices();
			} else {
				// Call user callback (onConnectionFailure)
				Log.d(TAG, "Connection failed");
				isConnected = false;
				mConnectionCallback.onConnectionFailure(mID,mHoof);
			}
		}

		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			mConnectedGatt = gatt;
			isConnected = true;
			
			// Call user callback (onConnectionSuccess)
			Log.d(TAG, "Device " + mID + " connected");
			mConnectionCallback.onConnectionSuccess(mID, mHoof);
		}

		@Override
		public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

			// State machine
			if (mSensor == ACCELEROMETER || mSensor == GYROSCOPE) {
				
				switch (mState) {
				
				case 0:
					setPeriod();
					break;
				
				case 1:
					enableNotifications();
					break;
				}
			}
		}

		@Override
		public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
			// Call user callback (onSetupComplete)	
			Log.d(TAG, "Setup complete");
			mSensorCallback.onSetupComplete(mID,mHoof, mSensor);
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
			
			// Accelerometer
			if(characteristic.getUuid().equals(SensorTagUUID.ACC_DATA)) {
				mSensorCallback.onValueReceived(mID, mHoof, ACCELEROMETER, characteristic.getValue());
			}
			
			// Gyroscope
			if(characteristic.getUuid().equals(SensorTagUUID.GYR_DATA)) {
				mSensorCallback.onValueReceived(mID, mHoof, GYROSCOPE, characteristic.getValue());
			}
			
		}
		
	};
		
}
