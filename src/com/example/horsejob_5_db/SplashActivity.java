package com.example.horsejob_5_db;

import com.example.horsejob_5_db.database.DbHelper;
import com.example.horsejob_5_db.tables.HorseTable;
import com.example.horsejob_5_db.tables.OwnerTable;


import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;


public class SplashActivity extends Activity implements OnClickListener {
	
	DbHelper dbHelper;
    SQLiteDatabase db;
    Cursor cursor;
    SimpleCursorAdapter adapter;
    
    Button addOwner;
    Button choseSelected;
    Button deleteSelected;
    
    ListView listView;
    
    
    public long chkdId = 0;
    
    static final String[] FROM = {OwnerTable.COLUMN_OWNER_NAME, OwnerTable.COLUMN_OWNER_PHONE, OwnerTable.COLUMN_OWNER_EMAIL};
	static final int[] TO = {R.id.text_ownerRow_name, R.id.text_ownerRow_phone, R.id.text_ownerRow_email};
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ActionBar ab = getActionBar();
        ab.hide();
        
        addOwner = (Button) findViewById(R.id.button_add_new_owner);
        addOwner.setOnClickListener(this);
        
        choseSelected = (Button) findViewById(R.id.button_select_marked_owner);
        choseSelected.setOnClickListener(this);
        choseSelected.setEnabled(false);
        
        deleteSelected = (Button) findViewById(R.id.button_delete_marked_owner);
        deleteSelected.setOnClickListener(this);
        deleteSelected.setEnabled(false);
        
        
        listView = (ListView) findViewById(R.id.listViewOwnerActivity);
        
        dbHelper = new DbHelper(this);
        
        
       
    }


    @Override
	protected void onResume() {
		Log.i("DB", "Splash onResume");
		updateList();
		
		for(int i = 0; i < listView.getAdapter().getCount(); i++){
			if(!listView.isItemChecked(i)){
				deleteSelected.setEnabled(false);
				choseSelected.setEnabled(false);
			}
		}
		
		
		super.onResume();
	}


	@Override
	protected void onPause() {
		Log.i("DB", "Splash onPause");
		super.onPause();
	}


	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


	
    @Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.button_add_new_owner:
			Intent intent = new Intent(this,com.example.horsejob_5_db.AddOwner.class);
			startActivity(intent);
			break;
			
		case R.id.button_select_marked_owner:
			Intent i = new Intent(this, com.example.horsejob_5_db.OwnerActivity.class);
			i.putExtra("key",chkdId);
			startActivity(i);
			break;
			
		case R.id.button_delete_marked_owner:
			showDeletAlertDialog();
			break;
		}
		
	}
    public void updateList(){

    	db = dbHelper.getReadableDatabase();
    	cursor = db.query(OwnerTable.TABLE_NAME, null,null,
    			null,null,null, OwnerTable.COLUMN_OWNER_NAME +" ASC;");

    	adapter = new SimpleCursorAdapter(this,android.R.layout.simple_list_item_single_choice, cursor, 
    			FROM, new int[] {android.R.id.text1},
    			SimpleCursorAdapter.NO_SELECTION);

    	listView.setAdapter(adapter);
    	listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

    	listView.setOnItemClickListener(new OnItemClickListener() {

    		@Override
    		public void onItemClick(AdapterView<?> parent, View view,
    				int position, long id) {

    			choseSelected.setEnabled(true);
    			deleteSelected.setEnabled(true);
    			chkdId = id;
    			Log.i("DB","Item at position: " +position +" clicked with id: " +chkdId );
    		}

    	});
    	db.close();
    }
    
    public void showDeletAlertDialog(){
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setTitle("ALERT!");
    	
    	final TextView mDialogMessage = new TextView(this);
    	String post = new String();
    	
    	db = dbHelper.getReadableDatabase();
    	cursor = db.query(OwnerTable.TABLE_NAME, new String[] {OwnerTable.COLUMN_OWNER_NAME}, OwnerTable.COLUMN_OWNER_ID +" = "+ chkdId, null, null, null, null);
    	cursor.moveToFirst();
    	
    	post = cursor.getString(cursor.getColumnIndex(OwnerTable.COLUMN_OWNER_NAME));
    	
    	mDialogMessage.setPadding(100, 0, 100, 0);
    	mDialogMessage.setText("Do you really want to delete this owner: \n"+post+"?\n" 
    			+"This will also delete all horses related to this owner.");


    	builder.setView(mDialogMessage)

    	.setPositiveButton("YES", new DialogInterface.OnClickListener() {

    		@Override
    		public void onClick(DialogInterface dialog, int which) {
    			try{
    				db.close();
    			}finally{
    				db = dbHelper.getWritableDatabase();
    			}try{
    				db.delete(HorseTable.TABLE_NAME, HorseTable.COLUMN_OWNER_ID + " = " +chkdId, null);
    			}finally{
    				try{
    					db.delete(OwnerTable.TABLE_NAME, OwnerTable.COLUMN_OWNER_ID +" = "+ chkdId, null);
    				}finally{
    					db.close();
    					dialog.cancel();
    					updateList();
    					deleteSelected.setEnabled(false);
    					choseSelected.setEnabled(false);
    				}
    			}
    		}})
    	
    	.setNegativeButton("NO", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				db.close();
				dialog.cancel();
			}
		})

    	.show();

    }
}
