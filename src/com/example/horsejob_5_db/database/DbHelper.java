package com.example.horsejob_5_db.database;



import com.example.horsejob_5_db.tables.HorseTable;
import com.example.horsejob_5_db.tables.LoggTable;
import com.example.horsejob_5_db.tables.NotesTable;
import com.example.horsejob_5_db.tables.OwnerTable;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper{
	
	private static final String DATABASE_NAME = "HorseJob";
	private static final int DATABASE_VERSION = 7;
	
	public DbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		OwnerTable.onCreate(db);
		HorseTable.onCreate(db);
		LoggTable.onCreate(db);
		NotesTable.onCreate(db);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		OwnerTable.onUpgrade(db, oldVersion, newVersion);
		HorseTable.onUpgrade(db, oldVersion, newVersion);
		LoggTable.onUpgrade(db, oldVersion, newVersion);
		NotesTable.onUpgrade(db, oldVersion, newVersion);
		
	}

}
