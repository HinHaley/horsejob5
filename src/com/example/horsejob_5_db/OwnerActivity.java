package com.example.horsejob_5_db;

import com.example.horsejob_5_db.database.DbHelper;
import com.example.horsejob_5_db.tables.HorseTable;
import com.example.horsejob_5_db.tables.OwnerTable;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class OwnerActivity extends Activity implements OnClickListener{
	
	//variables for database handling
	public long ownerId;
	public long chkdId;
	public static String[] OwnerInfo = new String[3];
	static final String[] FROM = {HorseTable.COLUMN_NAME};
	
	//Views
	TextView name;
	TextView phone;
	TextView email;
	ListView listView;
	Button addHorse;
	Button deleteHorse;
	Button choseHorse;
	
	DbHelper dbHelper;
	SQLiteDatabase db;
	Cursor cursor;
	SimpleCursorAdapter adapter;
	Handler mHandler;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_owner);
		ActionBar ab = getActionBar();
		ab.hide();
		
		//Initiate dbHelper
		dbHelper = new DbHelper(this);
		
		//Get owner id
		Intent i = getIntent();
		Bundle b = i.getExtras();
		if(b!=null) ownerId = b.getLong("key");
		//ListView
		listView = (ListView) findViewById(R.id.listViewOwnerActivity);
		//update ListView
//		updateList();
		//owner info views.
		name = (TextView) findViewById(R.id.text_owner_activity_owner_name);
		phone = (TextView) findViewById(R.id.text_owner_activity_owner_phone);
		email = (TextView) findViewById(R.id.text_owner_activity_owner_email);
		
		//Buttons
		addHorse = (Button) findViewById(R.id.button_owner_activity_add_horse);
		deleteHorse = (Button) findViewById(R.id.button_owner_activity_delete_horse);
		choseHorse = (Button) findViewById(R.id.button_owner_activity_chose_horse);
		
		addHorse.setOnClickListener(this);
		deleteHorse.setOnClickListener(this);
		choseHorse.setOnClickListener(this);
		
		deleteHorse.setEnabled(false);
		choseHorse.setEnabled(false);
		
		//get owner info
		OwnerInfo = getOwnerInfo(ownerId);
		//display owner info.
		name.setText(OwnerInfo[0]);
		phone.setText(OwnerInfo[1]);
		email.setText(OwnerInfo[2]);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.owner, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private String[] getOwnerInfo(long id){
		Log.i("DB", "getOwnerInfo() called");
		String[] info = new String[3];
		
		db = dbHelper.getReadableDatabase();
		
		cursor = db.query(OwnerTable.TABLE_NAME, 
				new String[] {OwnerTable.COLUMN_OWNER_NAME,
				OwnerTable.COLUMN_OWNER_PHONE, OwnerTable.COLUMN_OWNER_EMAIL}, 
				OwnerTable.COLUMN_OWNER_ID +" = "+ id, 
				null, null, null, null);
    	
		cursor.moveToFirst();
    	info[0] = cursor.getString(cursor.getColumnIndex(OwnerTable.COLUMN_OWNER_NAME));
    	cursor.moveToFirst();
		info[1] = cursor.getString(cursor.getColumnIndex(OwnerTable.COLUMN_OWNER_PHONE));
		cursor.moveToFirst();
		info[2] = cursor.getString(cursor.getColumnIndex(OwnerTable.COLUMN_OWNER_EMAIL));
		
		Log.i("DB", info[0]+" "+info[1]+" "+info[2]);
		db.close();
		return info;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.button_owner_activity_add_horse:
			Intent intent = new Intent(this, AddHorse.class);
			intent.putExtra("key", ownerId);
			startActivity(intent);
			break;
		case R.id.button_owner_activity_delete_horse:
			break;
		case R.id.button_owner_activity_chose_horse:
			Intent i = new Intent(this, HorseActivity.class);
			i.putExtra("key1", chkdId);
			startActivity(i);
			break;
		}
	}
	
	private void updateList(){ 
		
		try{
			db = dbHelper.getReadableDatabase();
		}catch (SQLException e){
			Log.i("DB", e.toString());
		}
		
		cursor = db.query(HorseTable.TABLE_NAME,new String[]{HorseTable.COLUMN_ID, HorseTable.COLUMN_NAME},
				HorseTable.COLUMN_OWNER_ID + " = " + ownerId ,
				null,null,null, HorseTable.COLUMN_NAME +" ASC;");
		Log.i("DB", "updateList called in OwnerActivity with: " +ownerId);
		
		adapter = new SimpleCursorAdapter(this,android.R.layout.simple_list_item_single_choice, cursor, 
				FROM, new int[] {android.R.id.text1},
				SimpleCursorAdapter.NO_SELECTION);

		listView.setAdapter(adapter);
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				choseHorse.setEnabled(true);
				deleteHorse.setEnabled(true);
				chkdId = id;
				Log.i("DB","Item at position: " +position +" clicked with id: " +chkdId );
			}

		});
		db.close();
		
	}

	@Override
	protected void onResume() {
		Log.i("DB","OwnerActivity onResume");
		deleteHorse.setEnabled(false);
		choseHorse.setEnabled(false);
		updateList();
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.i("DB","OwnerActivity onPause");
		super.onPause();
	}
}
